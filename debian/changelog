refcard (12.0) unstable; urgency=medium

  * Add entry for live images.
  * Formatting changes, to get plain quotes instead of curly quotes, making
    copy-and-paste of command possible/easier.
  * Fix over-long line.

  * Updated translations:
     * Bulgarian (Kamen Naydenov)
     * Catalan
     * Dutch (Frans Spiesschaert)
     * French (Baptiste Jammet)
     * Italian (Beatrice Torracca)
     * Indonesian (Kemas Antonius)
     * German (Holger Wansing)
     * Korean (Sangdo Jun)
     * Portuguese (Miguel Figueiredo)
     * Brazilian Portuguese (Adriano Rafael Gomes)
     * Swedish (Andreas Rönnquist)

 -- Holger Wansing <hwansing@mailbox.org>  Wed, 22 Mar 2023 22:15:43 +0100

refcard (11.0) unstable; urgency=medium

  * Refer to ifupdown for activating/deactivating network interfaces.
    Closes: #975420
  * Let's release for Bullseye.

 -- Holger Wansing <hwansing@mailbox.org>  Sun, 14 Mar 2021 16:17:33 +0100

refcard (10.7) unstable; urgency=medium

  [ Holger Wansing ]
  * dblatex corrupts visible URL to GPL, since it adds a hyphen at line-break.
    Include it in <filename>..</filename>, to avoid that.
  * Correct some wrong line-breaks in translations.
  * Add Korean translation by sebul <sebuls@gmail.com>.
  * Add texlive-lang-chinese and -korean as build-depends (additionally needed
    for CJK PDFs starting from texlive-2020).

  [ Alban Vidal ]
  * Add readme file.

  * Updated translations:
     * Bulgarian (Kamen Naydenov)
     * Simplified Chinese (肖盛文)
     * Traditional Chinese (Shen-Ta Hsieh)
     * Czech (Branislav Makúch)
     * Dutch (Frans Spiesschaert)
     * French (Baptiste Jammet)
     * German (Holger Wansing)
     * Italian (Beatrice Torracca)
     * Japanese (Victory)
     * Brazilian Portuguese (Adriano Rafael Gomes)
     * Portuguese (Miguel Figueiredo)
     * Swedish (Andreas Rönnquist)
     * Vietnamese (Trần Ngọc Quân)

 -- Holger Wansing <hwansing@mailbox.org>  Sat, 06 Jun 2020 19:07:58 +0200

refcard (10.6) unstable; urgency=medium

  * Replace no-longer-existing pdfjoin and pdfnup by their corresponding
    pdfjam commands. (Closes: #948801)

 -- Holger Wansing <hwansing@mailbox.org>  Sun, 19 Jan 2020 17:27:39 +0100

refcard (10.5) unstable; urgency=medium

  * Get rid of Build-Depend: dia (add a static refcard.png to the repo,
    while removing the 'dia' call).
  * Updated translations:
     * Italian (Dionisio E Alonso)

 -- Holger Wansing <hwansing@mailbox.org>  Sat, 26 Oct 2019 00:19:16 +0200

refcard (10.4) unstable; urgency=medium

  * Updated translations:
     * Finnish (Juhani Numminen)
     * Italian (Beatrice Torracca)
     * Japanese (Victory)

 -- Holger Wansing <hwansing@mailbox.org>  Thu, 07 Mar 2019 21:16:55 +0100

refcard (10.3) unstable; urgency=medium

  * Improve layout for some languages, to get the copyright notice at the
    bottom of the middle of first sheet. (Closes: #917766)
  * Use a smaller font for Greek, to get the copyright notice displayed.
    (Closes: #918127)
  * Use bigger font for Bulgarian, French and Spanish; improves readibility
    without any disadvantage. (Closes: #917768, #918773)
  * Replace all <literal> </literal> tags by <filename> </filename>, to fix
    missing line-breaks within the first ones. (Closes: #880696)
  * Replace pdftk (removed from unstable) by his successor pdftk-java.
  * Replace /sbin/ip command by ip. (Closes: #917336)
  * Move no longer built languages (ar, he, ml) to attic/ directory.

  * Updated translations:
     * Bulgarian (Kamen Naydenov)
     * Norwegian Bokmal (Hans Fredrik Nordhaug)

 -- Holger Wansing <hwansing@mailbox.org>  Sun, 13 Jan 2019 22:33:46 +0100

refcard (10.2) unstable; urgency=medium

  [ W. Martin Borgert ]
  * Fix use of dblatex to build pdf. (Closes: #898560)

  [ Holger Wansing ]
  * Get rid of xmlroff dependency (also disables Arabic, Hebrew and Malayalam
    build). Closes: #917458
  * Update (english) content for Buster.
  * Updated translations
     * Danish (Joe Hansen)
     * French (Baptiste Jammet)
     * Brazilian Portuguese (Adriano Rafael Gomes)
     * Serbian (Dragan Filipovic)
     * Ukrainian (Asal Mirzaieva)

 -- Holger Wansing <hwansing@mailbox.org>  Thu, 03 Jan 2019 22:25:39 +0100

refcard (10.1) unstable; urgency=low

  [ Holger Wansing ]
  * Translation updates:
    - Swedish: Andreas Ronnquist. Closes: #872499
    - Dutch: Frans Spiesschaert. Closes: #874290
  * Update package description (Debian GNU/Linux -> Debian)
  * Revert version displayed in the card back to Jessie version, to get a
    Jessie refcard at debian.org/doc again.
  * Remove auto-apt from the card, it is no longer in Debian. Closes: #869755

  [ W. Martin Borgert ]
  * Move git repo to salsa.debian.org
  * Bump dh compat level
  * Bump standards version to 4.1.4 (no changes)

 -- W. Martin Borgert <debacle@debian.org>  Sat, 12 May 2018 21:06:09 +0000

refcard (10.0) unstable; urgency=low

  [ Holger Wansing ]
  * Change package dependency: pdfjam has been removed from unstable,
    was already an transitional package for and has been superseded by
    texlive-extra-utils. Closes: 867101
  * Update ifup|ifdown command to ip link set. Closes: 869485
  * Fix several issues (Debian GNU/Linux -> Debian; switch from debsums
    to dpkg -V; update links; updates/fixes for new apt command). 
    Closes: 753492
  * Some minor fixups in wording.
  * Translation updates:
    - Portuguese: Miguel Figueiredo. Closes: #869997
    - German: Holger Wansing.
  * Change versioning scheme from 10.0.0 to 10.0.

  [ W. Martin Borgert ]
  * Remove fop from Makefile and add variable for disabling languages
  * Fix metadata.xsl for newer pdftk
  * Move from CDBS to dh
  * Set compat level to 10
  * Bump standards version to 4.0.1 (no changes)

 -- W. Martin Borgert <debacle@debian.org>  Wed, 09 Aug 2017 06:49:11 +0000

refcard (9.0.4) unstable; urgency=low

  [ Holger Wansing ]
  * Fix translation errors in simplified Chinese. Closes: 864403

 -- Holger Wansing <holgerw@debian.org>  Thu, 08 Jun 2017 11:28:26 +0200

refcard (9.0.3) unstable; urgency=low

  [ Holger Wansing ]
  * Some more Depends are needed to fix 834677: fonts-ipafont-gothic and
    fonts-ipafont-mincho. (Closes: 834677)
  [ W. Martin Borgert ]
  * Add Holger to Uploaders.

 -- Holger Wansing <holgerw@debian.org>  Thu, 01 Sep 2016 20:38:11 +0000

refcard (9.0.2) unstable; urgency=low

  * Deactivate Malayalam translation (heavily outdated, has never been
    fully supported by translator).
  * Add missing build-dependency fonts-ipaexfont-gothic (Closes: 834677).

 -- Holger Wansing <holgerw@debian.org>  Sun, 14 Aug 2016 20:15:19 +0000

refcard (9.0.1) unstable; urgency=medium

  * Drop dependency on font removed from Debian (Closes: #833265).
  * Minor update to debian/README.source.

 -- W. Martin Borgert <debacle@debian.org>  Sun, 07 Aug 2016 14:28:19 +0000

refcard (9.0) unstable; urgency=low

  * Team upload

  [ David Prévot ]
  * Get rid of now useless texlive-lang-greek build-dependency
    (workaround #725928 now fixed)

  [ Holger Wansing ]
  * Fix licence info (follow up to #681492)
  * Change versioning scheme, to make major version number match the
    major number of the relevant Debian release.
  * Update content (apt, sysvinit/systemd, console config) to follow
    development in Stretch; drop Apache/Samba/m-a/make-kpkg content.
  * Fix the use of wrong encoding in PDF metadata: use pdftk with 
    'update_info_utf8' instead of 'update_info'
  * Don't translate the command 'reportbug' in zh_CN (Closes: #783849)
  * Fix some typos in spanish translation, thanks to Octavio Alvarez
    (Closes: #794800)
  * Change build mechanism for cs, el and sk: use dblatex instead of
    xmlroff (Closes: #821119, #606421)
  * Change build mechanism for ja, zh-CN, zh-TW and hi: use dblatex
    instead of xmlroff; patch origin by Victory. (Closes: #783848, #832836)
  * Fix layout for RTL languages; patch by Omer Zak. (see #822083)
  * Translation updates:
    - German: Holger Wansing.
    - Japanese: Victory.
    - Vietnamese: Trần Ngọc Quân
    - Hebrew: Omer Zak
    - Czech: Miroslav Kure (Closes: #820516)
    - Russian: Yuri Kozlov (Closes: #820574)
    - Chinese: yanliang tang
    - Indonesian: Kemas Antonius
    - Portuguese: Miguel Figueiredo (Closes: #821032)
    - French: Baptiste Jammet (Closes: #821280)
    - Greek: Iliana Panagopoulou
    - Spanish: Octavio Alvarez
    - Slovak: helix84 (Closes: #822953)
    - Danish: Joe Dalton (Closes: #822898)
    - Catalan: Innocent De Marchi (Closes: #823242)
    - Bulgarian: Kamen Naydenov
    - Brazilian Portuguese: Adriano Rafael Gomes (Closes: #823336)
    - Dutch: Frans Spiesschaert (Closes: #823357)
    - Hungarian: Miklos Quartus
    - Traditional Chinese: Shen-Ta Hsieh
    - Norwegian Bokmål: Hans Fredrik Nordhaug
    - Basque: dooteo
    - Finnish: Juhani Numminen
    - Swedish: Andreas Ronnquist (Closes: #825300)
    - Polish: Mirosław Gabruś and Tomasz Nitecki (Closes: #825570)
    - Ukrainian: Asal Mirzaieva and Сергій Кондрашов
    - Hindi: Rajiv Ranjan
  * Translations added:
    - Serbian: Dragan Filipovic

  [ Beatrice Torracca ]
  * Update Italian translation

  [ W. Martin Borgert ]
  * Add missing build-deps
  * Moved packaging to Git
  * Bump standards version to 3.9.8, no changes
  * Source format 3.0 (native)
	
 -- W. Martin Borgert <debacle@debian.org>  Sun, 31 Jul 2016 11:57:22 +0000

refcard (5.0.9) unstable; urgency=low

  * Team upload

  [ Osamu Aoki ]
  * Fix version in the generated PDF and HTML (Closes: #681359)
  * Drop `Condition ... in legal notice and made it one paragraph
    to ensure GPL-3 link information (Closes:  #681492)
  * URL link to http://www.debian.org/doc/user-manuals#refcard

  [ David Prévot ]
  * Allow xmlroff to continue after any formatting errors (Closes: #713777)
  * Build-depend on fonts-ipafont-gothic instead of otf-ipafont-gothic
    (Closes: #714055)
  * Bump standards version to 3.9.4
  * Update Vcs to canonical URI
  * Update Homepage
  * Drop useless version in dependencies
  * Add some now needed texlive-lang-* dependencies

  [ Beatrice Torracca ]
  * Update Italian translation

  [ Victory ]
  * Update Japanese translation

 -- David Prévot <taffit@debian.org>  Wed, 09 Oct 2013 17:48:08 -0400

refcard (5.0.8) unstable; urgency=low

  * Team upload.
 
  [ W. Martin Borgert ]
  * Translation added:
    - Indonesian: Kemas Antonius <kyantonius@gmail.com>

  [ David Prévot ]
  * Update Reference Card URL. (Closes: #647062)
  * Fix Swedish typo, thanks to Andreas Rönnquist. (Closes: #668317)
  * Fix English typo, thanks to Holger Wansing. (Closes: #630924)
  * Add xz to (de)compressing tools, thanks to Cesar Gil. (Closes: #649413)
  * dblatex.xsl: change <xsl:choose> for de, it and ro, thanks to Holger
    Wansing. (Closes: #630923)

  [ Osamu Aoki ]
  * texlive-xetex (2012.20120611-3) lists tipa in Depends and fixed build
    problem.  (Closes: #666622)
  * Use proper native package version system.
  * Bump Standards-Version: 3.9.3 and add compat.

 -- Osamu Aoki <osamu@debian.org>  Tue, 10 Jul 2012 22:51:24 +0900

refcard (5.0.7-1) unstable; urgency=low

  * Team upload.
 
  [ Simon Paillard ]
  * Translation updates:
    - Dutch: Paul Gevers. (Closes: #606751)
  * Fix translations mess about editor/rm/pager command.
    Thanks a lot to Hans F. Nordhaug for the notice. 

 -- Simon Paillard <spaillard@debian.org>  Mon, 13 Dec 2010 22:57:47 +0100

refcard (5.0.6-1) unstable; urgency=low

  * Team upload.

  [ Simon Paillard ]
  * Translation updates:
    - Arabic: Ossama Khayat. (Closes: #605495)
    - Bulgarian: Kamen Naydenov.
    - Chinese: Tetralet. 
    - Chinese: yanliang tang.
    - Czech: Miroslav Kure. (Closes: 605927)
    - Danish: Kåre Thor Olsen.
    - Finnish: Tapio Lehtonen. 
    - German: Holger Wansing. (Closes: #606153)
    - Hebrew: Omer Za.
    - Norwegian: Hans F. Nordhaug.
    - Portuguese: Miguel Figueiredo.
    - Russian: Yuri Kozlov. (Closes: #592642)
    - Slovak: Ivan Masár. (Closes: #606071)
    - Spanish: Gunnar Wolf. 
    - Turkish: Selçuk Mıynat.
  * Add otf-ipafont-gothic to build-deps for correct japanese PDF with cairo
  * Update for grub2 user conf path (Closes: #605668)

  [ David Prévot ]
  * French Translation update.

  [ W. Martin Borgert ]
  * Disabled xmlroff gnomeprint backend in Makefile to prevent crashes
    (Closes: #584350). So I hope. Tested in a clean sid chroot.
  * Specify debian/source/format is 1.0.

 -- Simon Paillard <spaillard@debian.org>  Thu, 09 Dec 2010 20:47:49 +0100

refcard (5.0.5-3) unstable; urgency=low

  * Add build dependency on ghostscript. Closes: #577907.

 -- W. Martin Borgert <debacle@debian.org>  Tue, 20 Apr 2010 05:35:52 +0000

refcard (5.0.5-2) unstable; urgency=low

  * Do cs and sk languages with xmlroff instead of dblatex
    (closes: 551229) and add texlive-xetex, texlive-lang-cyrillic
    as build dependencies. Thanks to Lucas Nussbaum and Mike T.
  * Add Malay (ms) translation by Umarzuki Mochlis. Thanks!

 -- W. Martin Borgert <debacle@debian.org>  Sun, 21 Feb 2010 12:16:56 +0000

refcard (5.0.5-1) unstable; urgency=low

  * Fix missing Build-Depends on lmodern (closes: #512513).
    Thanks to Lucas Nussbaum.
  * Add translation by Tetralet (zh_TW). Thanks, too!
  * Fix four missing spaces in French po file.

 -- W. Martin Borgert <debacle@debian.org>  Fri, 23 Jan 2009 23:13:37 +0000

refcard (5.0.4-1) unstable; urgency=low

  * Now all 32 languages are complete. Thanks to the translators:
    David Planella (ca), Anders Wegge Keller and Kåre Thor Olsen (da),
    Rafael Ávila Coya (gl), Omer Zak (he), Paul Gevers (nl),
    Selçuk Mıynat (tr), and Artem B. (uk).
  * Use DEB_MAKE_ENVVARS, not DEB_MAKE_INSTALL_TARGET for USE_DBLATEX.

 -- W. Martin Borgert <debacle@debian.org>  Wed, 05 Nov 2008 09:00:07 +0000

refcard (5.0.3-1) unstable; urgency=low

  * New translations: Thanks to Ossama M. Khayat (ar), Rajiv Ranjan
    (hi)
  * Translation updates. Thanks to Emmanuel Galatoulas (el), Jean-Luc
    Coulon (fr), Miguel Figueiredo (pt)
  * This is the first time, that the reference card is fully
    translated into the nine "world languages". Many thanks to all
    translators of all languages, of course.

 -- W. Martin Borgert <debacle@debian.org>  Sun, 05 Oct 2008 17:22:03 +0000

refcard (5.0.2-1) unstable; urgency=low

  * Translations updated. Thanks to Kamen Naydenov (bg), Piarres
    Beobide (eu), Johan Haggi (it), shinichi tsunoda (ja), Hans F.
    Nordhaug (nb), Wiktor Wandachowicz (pl), Ionel Mugurel Ciobica
    (ro), Martin Bagge (sv), Deng Xiyue (zh_CN)
  * Minor layout fixes.

 -- W. Martin Borgert <debacle@debian.org>  Thu, 18 Sep 2008 22:16:41 +0000

refcard (5.0.1-1) unstable; urgency=low

  * Translations updated. Thanks to Tapio Lehtonen (fi), Johan
    Haggi (it), Eder L. Marques (pt_BR).
  * Reenabled eu, ja, sv by use of gp backend instead of cairo.
  * New version: Better layout by use of dblatex for some languages.

 -- W. Martin Borgert <debacle@debian.org>  Fri, 01 Aug 2008 19:29:09 +0000

refcard (5.0-1) unstable; urgency=low

  * New version with some updates for lenny (closes: #490918).
    Thanks to Justin B Rye for corrections and many useful ideas.
  * Translations updated. Thanks to Jens Seidel and Martin Zobel-Helas (de),
    Gunnar Wolf (es), Jean-Luc Coulon (fr), SZERVÁC Attila (hu), Kęstutis
    Biliūnas (lt), Hans F. Nordhaug (nb), Yuri Kozlov (ru), Peter Mann (sk),
    Martin Bagge (sv), Clythie Siddall (vi), (closes: #492346, #492080).
  * Reset original homepage (closes: #491528). Thanks to Yuri Kozlov.
  * fix hyperlinks (closes: #490815). Thanks again to Justin B Rye.
  * Note: eu, ja, sv temporarily disabled because of tool problems. Sorry!
    Under investigation.

 -- W. Martin Borgert <debacle@debian.org>  Fri, 25 Jul 2008 22:01:56 +0000

refcard (0.3.2-1) unstable; urgency=low

  * Initial package (closes: #463069).

 -- W. Martin Borgert <debacle@debian.org>  Thu, 31 Jan 2008 18:56:36 +0000
